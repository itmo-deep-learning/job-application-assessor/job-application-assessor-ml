import pandas as pd
from sklearn.metrics.pairwise import cosine_similarity
import re
import fasttext
import numpy as np
from huggingface_hub import hf_hub_download
import faiss

DELIMITER = ';'

model_path = hf_hub_download(
    repo_id="facebook/fasttext-ru-vectors", filename="model.bin")
model_fasttext = fasttext.load_model(model_path)


def get_tags(text):
    position_regex = re.compile(r"Должность: (.*?)\n")
    keys_regex = re.compile(r"(?:Keys|Навыки): \[(.*?)\]")
    experience_regex = re.compile(r"(?:Experience|Опыт): (.*?)\n")
    schedule_regex = re.compile(r"(?:Schedule|График): (.*?)\n")

    position_match = position_regex.search(text)
    keys_match = keys_regex.search(text)
    experience_match = experience_regex.search(text)
    schedule_match = schedule_regex.search(text)

    position = position_match.group(1) if position_match else ''
    keys = keys_match.group(1) if keys_match else ''
    experience = experience_match.group(1) if experience_match else ''
    schedule = schedule_match.group(1) if schedule_match else ''

    return 'Должность: ' + position + '\nНавыки: ' + keys + \
        '\nОпыт: ' + experience + '\nГрафик: ' + schedule


def get_embeddings_fasttext(sentences, model):
    embeddings = [model.get_sentence_vector(
        sentence) for sentence in sentences]
    return np.array(embeddings)


def get_data(path_to_csv: str):
    data = pd.read_csv(path_to_csv)
    data['vac'] = 'Должность: ' + \
        data['должность'] + '\n' + data['описание']
    data['inf'] = [get_tags(text) for text in data['vac']]
    lines = [line.replace('\n', DELIMITER) for line in data['inf']]
    embeddings = get_embeddings_fasttext(lines, model_fasttext)
    return (data, embeddings)


def get_job_application_embedding(res):
    res_inf = get_tags(res)
    res_line = res_inf.replace('\n', DELIMITER)
    res_embedding = model_fasttext.get_sentence_vector(res_line)
    return res_embedding


vacan_data = None
vacan_embeddings = None
normalized_vectors = None
index = None


def find_job_applications_fast(res):
    global vacan_data, vacan_embeddings, index, normalized_vectors

    if (vacan_data is None or vacan_embeddings is None):
        (data, embeddings) = get_data(
            path_to_csv='/home/n4d/courses/job-application-assessor-ml/data/hh_ru_jobs_concat.csv')
        vacan_data = data
        vacan_embeddings = embeddings

    if (index is None):
        normalized_vectors = vacan_embeddings / \
            np.linalg.norm(vacan_embeddings, axis=1)[:, np.newaxis]
        index = faiss.IndexFlatIP(300)
        index.add(normalized_vectors)

    res_inf = get_tags(res)
    res_line = res_inf.replace('\n', DELIMITER)

    res_embedding = model_fasttext.get_sentence_vector(res_line)
    normalized_query_vector = res_embedding / np.linalg.norm(res_embedding)

    distances, indices = index.search(
        np.array([normalized_query_vector]).astype('float32'), 3)

    return [vacan_data['vac'][idx] for idx in indices[0].tolist()]


def find_job_applications(res):
    global vacan_data, vacan_embeddings

    if (vacan_data is None or vacan_embeddings is None):
        (data, embeddings) = get_data(
            path_to_csv='/home/n4d/courses/job-application-assessor-ml/data/hh_ru_jobs_concat.csv')
        vacan_data = data
        vacan_embeddings = embeddings

    res_inf = get_tags(res)
    res_line = res_inf.replace('\n', DELIMITER)

    res_embedding = model_fasttext.get_sentence_vector(res_line)
    cosine_similarities = cosine_similarity(
        [res_embedding], vacan_embeddings)[0]
    top_matches_indices = cosine_similarities.argsort()[-3:][::-1]
    top_vacancies = [vacan_data['vac'][idx]
                     for idx in top_matches_indices]
    return top_vacancies
